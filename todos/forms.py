from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = "__all__"


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = "__all__"
